### Build and install packages
FROM python:3.6

COPY . .

ENTRYPOINT [ "python" ]
CMD ["-m", "http.server", "1337"]
